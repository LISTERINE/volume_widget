# Volume Widget

A minimal volume info widget for AwesomeWM.

Displays colorized volume based on info from alsa.

Images (mute to max)(sorry about the quality):

![Mute](https://gitlab.com/LISTERINE/volume_widget/raw/master/res/mute.png)
![Mute](https://gitlab.com/LISTERINE/volume_widget/raw/master/res/green.png)
![Mute](https://gitlab.com/LISTERINE/volume_widget/raw/master/res/orange.png)
![Mute](https://gitlab.com/LISTERINE/volume_widget/raw/master/res/red.png)

### Usage:

1. Clone this repository
2. Add the following to your rc.lua:

```
local volume_widget = require("volume-widget")
local volume = volume_widget({})
```