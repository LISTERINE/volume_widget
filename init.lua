-- Battery widget

local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local naughty = require("naughty")

local timer = gears.timer or timer
local watch = awful.spawn and awful.spawn.with_line_callback

------------------------------------------
-- Private utility functions
------------------------------------------

local function color(color, str)
    return '<span color="' .. color .. '">' .. str ..  '</span>'
end

------------------------------------------
-- Battery widget interface
------------------------------------------

local volume_widget= {}

function volume_widget:new(args)
    return setmetatable({}, {__index = self}):init(args)
end

function volume_widget:init(args)
    self.full_part = ":"
    self.half_part = "."

    self.widget = wibox.widget.textbox()
    self.widget.set_align("right")
    --self.widget.font = args.widget_font
    self.tooltip = awful.tooltip({objects={self.widget}})

    self.widget:buttons(awful.util.table.join(
        awful.button({ }, 1, function() self:update() end),
        awful.button({ }, 3, function() self:update() end)
    ))

    self.timer = timer({ timeout = args.timeout or 1 })
    self.timer:connect_signal("timeout", function() self:update() end)
    self.timer:start()
    self:update()

    return self
end


function volume_widget:generate_meter(full_parts, half_parts)
    local full_parts_str = string.rep(":", full_parts)
    local half_parts_str = string.rep(".", half_parts)
    local meter = full_parts_str .. half_parts_str
    if meter == "" then
	    return "[ - ]"
    end
    local green = color("green", string.sub(meter, 1, 5))
    local orange = color("orange", string.sub(meter, 6, 8))
    local red = color("red", string.sub(meter, 9, 10))
    local colorized  = green .. orange .. red
    if meter:len() < 5 then
	    return color("green", "[") .. colorized .. color("green", "]")
    elseif meter:len() < 9 then
	    return color("orange", "[") .. colorized .. color("orange", "]")
    else
	    return color("red", "[") .. colorized .. color("red", "]")
    end
end

function volume_widget:get_state()
    local state = {
	raw_level = 0,
	level = 0,
	full_parts = 0,
	half_parts = 0,
	graph_string = "[ ... ]"
    }
    local handle = io.popen("amixer sget Master")
	if not handle then
		state.graph_string = color("red", "[ alsa-err ]")
		return state
	else
		local raw_level, junk = handle:read("*a"):match("(%d*)%%")
		if raw_level == nil then
			state.graph_string = color("orange", "[ lvl-err ]")
			return state
		end
		local graph_parts = math.ceil((raw_level/2)/5)
		local full_parts = graph_parts
		local half_parts = 1
		if math.fmod(graph_parts, 2) == 0 then
			half_parts = 0
			if full_parts < 0 then
				full_parts = 0
			end
		end
		local graph = self:generate_meter(full_parts, half_parts)
		local round_level = graph_parts*5

		state["raw_level"] = raw_level
		state["level"] = round_level
		state["full_parts"] = full_parts
		state["half_parts"] = half_parts
		state["graph_string"] = graph
	end

    return state
end

function volume_widget:update()
    local ctx = self:get_state()

    -- for use in functions
    ctx.obj = self

    -- update text
    self.widget:set_markup(ctx.graph_string)
    self.tooltip:set_text("Volume: " .. ctx.raw_level .. "%")

end


return setmetatable(volume_widget, {
    __call = volume_widget.new,
})
